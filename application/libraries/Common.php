<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once("PasswordHash.php");

class Common 
{

    public function nohtml($message) 
    {
        $message = trim($message);
        $message = strip_tags($message);
        $message = htmlspecialchars($message, ENT_QUOTES);
        return $message;
    }

	public function encrypt($password) 
    {
        $phpass = new PasswordHash(12, false);
        $hash = $phpass->HashPassword($password);
    	return $hash;
    }
	 function generer_mot_de_passe($nb_caractere = 12)
    {
        $mot_de_passe = "";
       
        $chaine = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ023456789+@!$%?&";
        $longeur_chaine = strlen($chaine);
       
        for($i = 1; $i <= $nb_caractere; $i++)
        {
            $place_aleatoire = mt_rand(0,($longeur_chaine-1));
            $mot_de_passe .= $chaine[$place_aleatoire];
        }

        return $mot_de_passe;   
     }
    public function randomPassword() 
    {
    	$letters = array(
            "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q",
            "r","s","t","u","v","w","x","y","z"
        );
    	$pass = "";
    	for($i=0;$i<10;$i++) {
    		shuffle($letters);
    		$letter = $letters[0];
    		if(rand(1,2) == 1) {
	    		$pass .= $letter;
    		} else {
	    		$pass .= strtoupper($letter);
    		}
    		if(rand(1,3)==1) {
    			$pass .= rand(1,9);
    		}
    	}
    	return $pass;
    }

    public function getAccessLevel($level) 
    {
        if($level == 0) {
            return "Member";
        } elseif($level == 1) {
            return "Moderator";
        } elseif($level == 2) {
            return "Power Moderator";   
        } elseif($level == 3) {
            return "User Admin";
        } elseif($level == 4) {
            return "Admin";
        } elseif($level == -1) {
            return "Banned";
        } else {
            return "Invalid Level";
        }
    }

    public function checkAccess($level, $required) 
    {
        $CI =& get_instance();
        if($level < $required) {
            $CI->template->error(
                "You do not have the required access to use this page. 
                You must be a ". $this->getAccessLevel($required)
                . "to use this page."
            );
        }
    }

    public function send_email_($subject, $body, $emailt) 
    {
        $CI =& get_instance();
        $CI->load->library('email');

        $CI->email->from($CI->settings->info->site_email, $CI->settings->info->site_name);
        $CI->email->to($emailt);

        $CI->email->subject($subject);
        $CI->email->message($body);

        $CI->email->send();
    }

	public function send_email($name, $from, $to, $subject,$message)
	{
		$config = Array(
		'protocol' => 'imap',
        'smtp_host' => 'ssl0.ovh.net',
        'smtp_port' => 143,
        'smtp_user' => 'ragnero@ebene.info',
        'smtp_pass' => 'ebene12345',
        'mailtype'  => 'html', 
        'charset' => 'utf-8',
		'crlf' => "\r\n",
		'newline' => "\r\n",
        'wordwrap' => TRUE
    );
	
		$message = "From $name, \n\n $message";
		
		$headers = 'From: '.$from.'' . "\r\n" .
			'Reply-To: '.$from.'' . "\r\n" .
			'Content-type: text/html; charset=utf-8' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		
		$this->load->library('email',$config);
		$this->email->from($from, $name);
		$this->email->to($to);
		$this->email->cc($headers);
		$this->email->bcc('');
		
		$this->email->subject($subject);
		$this->email->message($message);
		
		if ( !$this->email->send())
		{
			// Generate error
			echo "L'email n\'est pas envoyé!!";
		}
				
		echo $this->email->print_debugger();
		
	}
    public function check_mime_type($file) {
        return true;
    }

    public function replace_keywords($array, $message) {
        foreach($array as $k=>$v) {
            $message = str_replace($k, $v, $message);
        }
        return $message;
    }
    
    
    
    
    
  function fctaffichimage($img_Src, $W_max, $H_max) {
  	

		 if (file_exists($img_Src)) {
		   // ---------------------
		   // Lit les dimensions de l'image source
		   $img_size = getimagesize($img_Src);
		   $W_Src = $img_size[0]; // largeur source
		   $H_Src = $img_size[1]; // hauteur source
		   // ---------------------
		   if(!$W_max) { $W_max = 0; }
		   if(!$H_max) { $H_max = 0; }
		   // ---------------------
		   // Teste les dimensions tenant dans la zone
		   $W_test = round($W_Src * ($H_max / $H_Src));
		   $H_test = round($H_Src * ($W_max / $W_Src));
		   // ---------------------
		   // si l'image est plus petite que la zone
		   if($W_Src<$W_max && $H_Src<$H_max) {
		      $W = $W_Src;
		      $H = $H_Src;
		   // sinon si $W_max et $H_max non definis
		   } elseif($W_max==0 && $H_max==0) {
		      $W = $W_Src;
		      $H = $H_Src;
		   // sinon si $W_max libre
		   } elseif($W_max==0) {
		      $W = $W_test;
		      $H = $H_max;
		   // sinon si $H_max libre
		   } elseif($H_max==0) {
		      $W = $W_max;
		      $H = $H_test;
		   // sinon les dimensions qui tiennent dans la zone
		   } elseif($H_test > $H_max) {
		      $W = $W_test;
		      $H = $H_max;
		   } else {
		      $W = $W_max;
		      $H = $H_test;
		   }
		   // ---------------------
		 } else { // si le fichier image n existe pas
		      $W = 0;
		      $H = 0;
		 }
		 // ---------------------------------------------------
		 // Affiche : src="..." width="..." height="..." pour la balise img
		 echo ' src="'.$img_Src.'" width="'.$W.'" height="'.$H.'"';
		 // ---------------------------------------------------
	}
/**  // © Jérome Réaux : http://j-reaux.developpez.com - http://www.jerome-reaux-creations.fr
* *****
*/
  
// ---------------------------------------------------
// Fonction de REDIMENSIONNEMENT physique "PROPORTIONNEL" et Enregistrement
// ---------------------------------------------------
// retourne : true si le redimensionnement et l'enregistrement ont bien eu lieu, sinon false
// ---------------------
// La FONCTION : fctredimimage ($W_max, $H_max, $rep_Dst, $img_Dst, $rep_Src, $img_Src)
// Les paramètres :
// - $W_max : LARGEUR maxi finale --> ou 0
// - $H_max : HAUTEUR maxi finale --> ou 0
// - $rep_Dst : repertoire de l'image de Destination (déprotégé) --> ou '' (même répertoire)
// - $img_Dst : NOM de l'image de Destination --> ou '' (même nom que l'image Source)
// - $rep_Src : repertoire de l'image Source (déprotégé)
// - $img_Src : NOM de l'image Source
// ---------------------
// 3 options :
// A- si $W_max!=0 et $H_max!=0 : a LARGEUR maxi ET HAUTEUR maxi fixes
// B- si $H_max!=0 et $W_max==0 : image finale a HAUTEUR maxi fixe (largeur auto)
// C- si $W_max==0 et $H_max!=0 : image finale a LARGEUR maxi fixe (hauteur auto)
// Si l'image Source est plus petite que les dimensions indiquées : PAS de redimensionnement.
// ---------------------
// $rep_Dst : il faut s'assurer que les droits en écriture ont été donnés au dossier (chmod)
// - si $rep_Dst = ''   : $rep_Dst = $rep_Src (même répertoire que l'image Source)
// - si $img_Dst = '' : $img_Dst = $img_Src (même nom que l'image Source)
// - si $rep_Dst='' ET $img_Dst='' : on ecrase (remplace) l'image source !
// ---------------------
// NB : $img_Dst et $img_Src doivent avoir la meme extension (meme type mime) !
// Extensions acceptées (traitees ici) : .jpg , .jpeg , .png
// Pour Ajouter d autres extensions : voir la bibliotheque GD ou ImageMagick
// (GD) NE fonctionne PAS avec les GIF ANIMES ou a fond transparent !
// ---------------------
// UTILISATION (exemple) :
// $redimOK = fctredimimage(120,80,'reppicto/','monpicto.jpg','repimage/','monimage.jpg');
// if ($redimOK==true) { echo 'Redimensionnement OK !';  }
// ---------------------------------------------------
   function fctdeformimage($W_fin, $H_fin, $rep_Dst, $img_Dst, $rep_Src, $img_Src) {
   // Si certains paramètres ont pour valeur '' :
   if ($rep_Dst == '') { $rep_Dst = $rep_Src; } // (même répertoire)
   if ($img_Dst == '') { $img_Dst = $img_Src; } // (même nom)
 // ------------------------
 // si le fichier existe dans le répertoire, on continue...
 if (file_exists($rep_Src.$img_Src) && ($W_fin!=0 || $H_fin!=0)) { 
   // ------------------------
   // extensions acceptées : 
	$extension_Allowed = 'jpg,jpeg,png';	// (sans espaces)
   // extension fichier Source
	$extension_Src = strtolower(pathinfo($img_Src,PATHINFO_EXTENSION));
   // ------------------------
   // extension OK ? on continue ...
   if(in_array($extension_Src, explode(',', $extension_Allowed))) {
      // ------------------------
      // récupération des dimensions de l'image Src
      $img_size = getimagesize($rep_Src.$img_Src);
      $W_Src = $img_size[0]; // largeur
      $H_Src = $img_size[1]; // hauteur
      // ------------------------
      // condition de redimensionnement et dimensions de l'image finale
      // Dans TOUS les cas : redimensionnement non-proportionnel
      // ------------------------
      // A- LARGEUR ET HAUTEUR fixes
      if ($W_fin != 0 && $H_fin != 0) {
         $W = $W_fin;
         $H = $H_fin;
      }
      // ------------------------
      // B- HAUTEUR fixe
      if ($W_fin == 0 && $H_fin != 0) {
         $W = $W_Src;
         $H = $H_fin;
      }
      // ------------------------
      // C- LARGEUR fixe
      if ($W_fin != 0 && $H_fin == 0) {
         $W = $W_fin;
         $H = $H_Src;
      }
      // ------------------------------------------------
      // REDIMENSIONNEMENT
      // ------------------------------------------------
      // creation de la ressource-image "Src" en fonction de l extension
      switch($extension_Src) {
      case 'jpg':
      case 'jpeg':
        $Ress_Src = imagecreatefromjpeg($rep_Src.$img_Src);
        break;
      case 'png':
        $Ress_Src = imagecreatefrompng($rep_Src.$img_Src);
        break;
      }
      // ------------------------
      // creation d une ressource-image "Dst" aux dimensions finales
      // fond noir (par defaut)
      switch($extension_Src) {
      case 'jpg':
      case 'jpeg':
        $Ress_Dst = imagecreatetruecolor($W,$H);
        break;
      case 'png':
        $Ress_Dst = imagecreatetruecolor($W,$H);
        // fond transparent (pour les png avec transparence)
        imagesavealpha($Ress_Dst, true);
        $trans_color = imagecolorallocatealpha($Ress_Dst, 0, 0, 0, 127);
        imagefill($Ress_Dst, 0, 0, $trans_color);
        break;
      }
      // ------------------------------------------------
      // REDIMENSIONNEMENT (copie, redimensionne, re-echantillonne)
      imagecopyresampled($Ress_Dst, $Ress_Src, 0, 0, 0, 0, $W, $H, $W_Src, $H_Src); 
      // ------------------------------------------------
      // ENREGISTREMENT dans le repertoire (avec la fonction appropriee)
      switch ($extension_Src) { 
      case 'jpg':
      case 'jpeg':
        imagejpeg ($Ress_Dst, $rep_Dst.$img_Dst);
        break;
      case 'png':
        imagepng ($Ress_Dst, $rep_Dst.$img_Dst);
        break;
      }
      // ------------------------
      // liberation des ressources-image
      imagedestroy ($Ress_Src);
      imagedestroy ($Ress_Dst);
      // ------------------------
   }
 }
 // ---------------------------------------------------
 // retourne : true si le redimensionnement et l'enregistrement ont bien eu lieu, sinon false
 if (file_exists($rep_Dst.$img_Dst)) { return true; }
 else { return false; }
 // ---------------------------------------------------
} 
    
function verify_image_size($file ,$widthmin,$heightmin){
	
 	$img_size = getimagesize($file);
	$W_Src = $img_size[0]; // largeur source
 	$H_Src = $img_size[1]; // hauteur source
 	
 	if($W_Src >= $widthmin  && $H_Src >= $heightmin)return true ;
 	else return false ;
	}
 function verify_equal_image_size($file ,$widthmin,$heightmin){
	
 	$img_size = getimagesize($file);
	$W_Src = $img_size[0]; // largeur source
 	$H_Src = $img_size[1]; // hauteur source
 	
 	if($W_Src == $widthmin  && $H_Src == $heightmin)return true ;
 	else return false ;
	}   
function get_date_now()
{
	date_default_timezone_set('UTC');
			$datestring = "%Y-%m-%d";
			$datenow = date('Y-m-d H:i:00:00', time());
		    return $datenow ;
	}
}

?>
