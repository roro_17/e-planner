<?php

// Admin Panel Translations
$lang['login_1'] = "Connectez-vous &agrave; votre compte";
$lang['login_2'] = "Entrez vos informations";
$lang['login_3'] = "Email";
$lang['login_4'] = "Mot de passe";
$lang['login_5'] = "Mot de passe oubli&eacute; ?";
$lang['login_6'] = "Connectez-vous";
$lang['login_7'] = "&copy; 2015.";
$lang['login_8'] = "STOCKPARC";
$lang['login_9'] = "par";
$lang['login_10'] = "Finsofts";
$lang['login_11'] = "Opérateur inconnu !";
$lang['login_12'] = "Mot de passe incorrect";

?>